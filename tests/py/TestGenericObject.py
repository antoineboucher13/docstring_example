#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Authors : Antoine Boucher
GenericObject class
"""

# Futures and six

# Built-in/Generic Imports
import os
import sys
import unittest

# Libs
# Own modules
from src.py.GenericObject import GenericObject


class TestGenericObject(unittest.TestCase):
    def setUp(self):
        self.generic_object1 = GenericObject('1', 1, None)
        self.generic_object2 = GenericObject('2', 2, self.generic_object1)
        self.generic_object3 = GenericObject('3', 3, self.generic_object2)

    def test_cool_dictionary_method(self):
        self.assertDictEqual(self.generic_object1.cool_dictionary_method(),
                             {
                                 'cool_str': '1',
                                 'cool_int': self.generic_object1._arg2
                             })

        self.assertDictEqual(self.generic_object1.cool_dictionary_method('2'),
                             {
                                 'cool_str': '2',
                                 'cool_int': self.generic_object1._arg2
                             })

    def test_cool_dictionary_static_method(self):
        self.assertDictEqual(self.generic_object1.cool_dictionary_static_method('shit', 42),
                             {
                                 'cool_str': 'shit',
                                 'cool_int': 42
                             })

    def test_cool_dictionary_raise_method(self):
        self.assertDictEqual(self.generic_object1.cool_dictionary_raise_method('shit', 42),
                             {
                                 'cool_str': 'shit',
                                 'cool_int': 42
                             })
        with self.assertRaises(ValueError) as context:
            self.generic_object1.cool_dictionary_raise_method(42, 42)
        self.assertTrue("Please put a str in the arg1 parameter. currently: <class 'int'>" in str(context.exception))
        with self.assertRaises(ValueError) as context2:
            self.generic_object1.cool_dictionary_raise_method('shit', 'shit')
        self.assertTrue("Please put a int in the arg2 parameter. currently: <class 'str'>" in str(context2.exception))

    def test_cool_stuff_for_dict_method(self):
        self.assertTupleEqual(self.generic_object2.cool_stuff_for_dict_method({
            'cool_str': 'shit',
            'cool_int': 42
        }), ('shit', 42))

    def test_arg3_getter(self):
        self.assertEqual(self.generic_object3.arg3, self.generic_object2)

    def test_arg3_setter(self):
        self.generic_object3.arg3 = self.generic_object1
        self.assertEqual(self.generic_object3.arg3, self.generic_object1)
        self.generic_object3.arg3 = self.generic_object2

    def test__str__(self):
        self.generic_object3.arg3 = None
        self.assertEqual(self.generic_object3.__str__(), '<GenericObject (arg1: 3, arg2: 3, arg3: None)>')
        self.generic_object3.arg3 = self.generic_object2

    def test__repr__(self):
        self.assertEqual(self.generic_object3.__repr__(), self.generic_object3.__str__())

