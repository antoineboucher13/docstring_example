describe("test suite for index.js", function() {
    'use strict';
    it("add one to one", function() {
        expect(addOne(1)).toBe(2);
    });
    it("add two to one", function() {
        expect(addTwo(1)).toBe(3);
    });
    it("add three to one", function() {
        expect(addThree(1)).toBe(4);
    });
});