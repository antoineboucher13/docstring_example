# Docstring example with python

Imitate the journeyman and/or portal env to test and create documentation

CI/CD

- Bitbucket pipeline: [bitbucket pipline python](https://confluence.atlassian.com/bitbucket/python-with-bitbucket-pipelines-873891271.html)

Python

- sphinx: [sphinx-quickstart](http://www.sphinx-doc.org/en/master/usage/quickstart.html)
- sphinx docstring: [example](https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html)
- coverage.py: [coverage.py](https://coverage.readthedocs.io/en/v4.5.x/)

Images
Documentation PY

![documentation](images/sphinxpy.PNG)

Go to `docs/py/_build/index.html`

Coverage PY

![documentation](images/coveragepy.PNG)

Go to `coverage/py/index.html`


JS

- Karma: [config](https://karma-runner.github.io/4.0/config/configuration-file.html)
- PhantomJS: [PhantomJS](https://dzone.com/articles/using-phantomjs-with-karma)
- documentation: [documentation](https://github.com/documentationjs/documentation)

Images
Documentation JS

![documentation](images/documentationjs.PNG)

Go to `docs/js/index.html`

Coverage JS

![documentation](images/karmacoveragejs.PNG)

Go to `coverage/js/index.html`
 