// Karma configuration
// Generated on Mon Jul 17 2017 10:43:07 GMT-0400 (EDT)

module.exports = function (config) {
    'use strict';

    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        plugins: [
            require('karma-jasmine'),
            require('karma-phantomjs-launcher'),
            require('karma-coverage')
        ],

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            // vendor js

            // js files
            'src/js/index.js',
            //js tests
            'tests/js/index.spec.js'
        ],


        // list of files to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'src/js/index.js': ['coverage']
        },

        client:{
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'coverage'],

        //https://karma-runner.github.io/0.8/config/coverage.html
        coverageReporter: {
            type : 'html',
            dir : 'coverage/js'
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

//        customLaunchers: {
//            'FirefoxHeadless': {
//                base: 'Firefox',
//                flags: [
//                    '-headless'
//                ]
//            }
//        },

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],
        // browsers: ['Chrome', 'Firefox', 'Safari'],

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    });
};
