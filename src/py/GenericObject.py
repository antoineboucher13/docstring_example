#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Authors : Antoine Boucher
GenericObject class
"""

# Futures and six

# Built-in/Generic Imports


# Libs
# Own modules

# TODO add more STUFF
class GenericObject(object):
    """This a Generic Object to show the basic of sphinx documentation"""

    def __init__(self, arg1, arg2, arg3=None):
        """Constructor for GenericObject

        :param arg1: use in the cool key in the cool dictionary
        :type arg1: str
        :param arg2: use to create cool dictionary
        :type arg2: int
        :param arg3: random object use to show @property
        :type arg3: object
        """
        self._arg1 = arg1
        self._arg2 = arg2
        self._arg3 = arg3

    def cool_dictionary_method(self, arg1=''):
        """This method make a cool dictionary

        :param arg1: what to put in the cool dictionary
        :type arg1: str
        :return: cool dictionary containing cool shit

        .. code-block:: python

            {
                'cool_str': 'shit',
                'cool_int': 42
            }

        :rtype: dict
        """
        if arg1 == '':
            arg = self._arg1
        else:
            arg = arg1

        cool_dict = {
            'cool_str': arg,
            'cool_int': self._arg2
        }
        return cool_dict

    @staticmethod
    def cool_dictionary_static_method(arg1='shit', arg2=42):
        """This method make a cool dictionary

        :param arg1: a cool string to put in cool dictionary
        :type arg1: str
        :param arg2: a cool int to put in cool dictionary
        :type arg2: int
        :return: cool dictionary containing cool shit

        .. code-block:: python

            {'cool': arg1}

        :rtype: dict
        """
        cool_dict = {
            'cool_str': arg1,
            'cool_int': arg2
        }
        return cool_dict

    @staticmethod
    def cool_dictionary_raise_method(arg1, arg2):
        """This method make a cool dictionary and raise error if the value of the args is invalid

        :param arg1: a cool string to put in cool dictionary
        :type arg1: str
        :raise ValueError: if the arg1 is not a str
        :param arg2: a cool int to put in cool dictionary
        :type arg2: int
        :raise ValueError: if the arg1 is not a int
        :return: cool dictionary containing cool shit

        .. code-block:: python

            {
                'cool_str': 'shit',
                'cool_int': 42
            }

        :rtype: dict

        """
        if not isinstance(arg1, str):
            raise ValueError("Please put a str in the arg1 parameter. currently: {type}".format(type=type(arg1)))
        if not isinstance(arg2, int):
            raise ValueError("Please put a int in the arg2 parameter. currently: {type}".format(type=type(arg2)))
        cool_dict = {
            'cool_str': arg1,
            'cool_int': arg2
        }
        return cool_dict

    def cool_stuff_for_dict_method(self, cool_dict):
        """This method get the cool stuff in a cool dict

        :param cool_dict: a cool dictionary containing cool shit

            .. code-block:: python

                {
                    'cool_str': 'shit',
                    'cool_int': 42
                }

        :type cool_dict:
        :return: tuple containing the cool stuff in the cool dict

            .. code-block:: python

                ('shit', 42)

        :rtype: tuple
        """
        self._arg1 = cool_dict.get('cool_str')
        self._arg2 = cool_dict.get('cool_int')
        return self._arg1, self._arg2

    @property
    def arg3(self):
        """Getter for the @property arg3

        :return: arg3 value
        :rtype: object
        """
        return self._arg3

    @arg3.setter
    def arg3(self, arg):
        """Setter for the @property arg3

        :param arg: the new value of the self._arg3
        :type: object
        :return: None
        :rtype: None
        """
        self._arg3 = arg

    def __str__(self):
        """overwriting the __str__ function to be more accurate with the GenericObject used by__repr__"""

        return '<GenericObject (arg1: {arg1}, arg2: {arg2}, arg3: {arg3})>'.format(arg1=self._arg1, arg2=self._arg2, arg3=self._arg3)

    def __repr__(self):
        """the __str__ function"""
        return self.__str__()

