from GenericObject import GenericObject

if __name__ == "__main__":
    generic_object = GenericObject('shit', 42, None)
    main_generic_object = GenericObject('shit2', 84, generic_object)

    # main_generic_object arg3 @property
    print('getter arg3: {}\n'.format(generic_object.arg3.__repr__()))

    generic_object.arg3 = GenericObject('shit', 43, None)
    print('setter arg3: {}\n'.format(generic_object.arg3.__repr__()))

    generic_object.arg3 = GenericObject('shit', 42, None)
    print('getter arg3: {}\n'.format(generic_object.arg3.__repr__()))

    # cool_dict creator
    cool_dict = main_generic_object.cool_dictionary_method('shit')
    print('cool_dict: {dict}\n'.format(dict=cool_dict))

    cool_dict2 = main_generic_object.cool_dictionary_static_method('shit', 42)
    print('cool_dict: {dict}\n'.format(dict=cool_dict2))

    # cool_tuple from co0l_dict creator
    cool_tuple = main_generic_object.cool_stuff_for_dict_method(cool_dict2)
    print('cool_tuple: {tuple}\n'.format(tuple=cool_tuple))

    # cool_dict creator raise
    try:
        cool_dict3 = main_generic_object.cool_dictionary_raise_method(42, 'shit')
    except ValueError as ve:
        print()
        print('Error: {error}\n'.format(error=ve))
        pass
