/**
 * This function adds one to its input.
 * @param {number} input any number
 * @returns {number} that number, plus one.
 */
function addOne(input) {
    'use strict';
    return input + 1;
}

/**
 * This function adds two to its input.
 * @param {number} input any number
 * @returns {number} that number, plus two.
 */
function addTwo(input) {
    'use strict';
    return input + 2;
}

/**
 * This function adds three to its input.
 * @param {number} input any number
 * @returns {number} that number, plus two.
 */
function addThree(input) {
    'use strict';
    return input + 3;
}

addOne(1);
addTwo(1);
addThree(1);
