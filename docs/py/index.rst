.. GenericObject documentation master file, created by
   sphinx-quickstart on Wed Nov  6 15:20:56 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GenericObject's documentation!
=========================================

.. automodule:: GenericObject
    :members:

.. toctree::
   :maxdepth: 3
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
